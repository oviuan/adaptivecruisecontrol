package adaptivecruisecontrol;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class DashedStripe implements IGameObject
{
    public static int LANES = 4;
    public static int EDGE_OFFSET = 20;

    private int m_nSpeed = 0;
    
    private CarObject m_carObject;
    
    @Override
    public void update()
    {
	m_nSpeed += m_carObject.getSpeed();
    }
    
    @Override
    public void draw(Graphics g)
    {
	g.setColor(new Color(150, 150, 150));
	g.fillRect(EDGE_OFFSET - 6, 0, 15, Main.HEIGHT);
	g.fillRect(Main.WIDTH - EDGE_OFFSET - 6, 0, 15, Main.HEIGHT);
	
	for (int xIndex = 1; xIndex < LANES; xIndex++)
	{
	    int x = EDGE_OFFSET + xIndex * (Main.WIDTH - 2 * EDGE_OFFSET) / LANES;
	    //boolean edgeLine = xIndex == 0 || xIndex == LANES;
	    
	    for (int yIndex = -100; yIndex <= Main.HEIGHT + 100; yIndex += 100)
	    {
		int y = (((yIndex - 40 + m_nSpeed) + 100) % (Main.HEIGHT + 220)) - 100;
		g.fillRect(x, y, 10, 80);
	    }
	}
    }
    
    public void setCarObject(CarObject car)
    {
	m_carObject = car;
    }
}