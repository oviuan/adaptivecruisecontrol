package adaptivecruisecontrol;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class AccelerometerSensor extends ISensor
{
    private static String ACCELEROMETER_ACTIVATION_URI = "/sys/class/misc/FreescaleAccelerometer/enable";
    //@SuppressWarnings("FieldCanBeLocal")
    private static String ACCELEROMETER_DATA_URI = "/sys/class/misc/FreescaleAccelerometer/data";

    public static void enableSensor(boolean enable) throws Exception
    {
        File file = new File(ACCELEROMETER_ACTIVATION_URI);
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        
        bw.write(enable ? "1" : "0");
        bw.close();
    }

    public static boolean isEnabled()
    {
        try
        {
            return Integer.parseInt(read(ACCELEROMETER_ACTIVATION_URI)) == 1;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        
        return false;
    }

    public static int[] getData()
    {
	int[] result = new int[3];
	
        try
        {
            String[] data = read(ACCELEROMETER_DATA_URI).split(",");
            result[0] = Integer.parseInt(data[0]);
            result[1] = Integer.parseInt(data[1]);
            result[2] = Integer.parseInt(data[2]);
        }
        catch (Exception e)
        {
	    System.out.println(e.getMessage());
        }
        
        return result;
    }
}
