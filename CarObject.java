package adaptivecruisecontrol;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

public class CarObject implements IGameObject
{
    public static int KEY_SENSITIVITY = 2;
    public static float SPEED_TRACK_SENSITIVITY = 0.5f;
    public static float ACCELERATION = 0.05f;
    public static float TOP_SPEED = 10.0f;
    public static int SENSOR_RANGE = Main.HEIGHT / 2;
    public static int CAR_WIDTH = 80;
    public static int CAR_HEIGHT = 100;
	public static int MIN_SAFE_DISTANCE = 5;
    
    public boolean Left = false;
    public boolean Right = false;
    public boolean Up = false;
    public boolean Down = false;
    public boolean Accelerate = false;
    public boolean Brake = false;
    public boolean AutoPilot = false;
	public boolean AutoOvertaking = false;
    
    private float m_fXPos = 100;
    private float m_fYPos = 100;
    private float m_fSpeed = 0.0f;
    private float m_fTargetSpeed = 0.0f;
	private float m_fTargetXPos;
    private int m_nSensorReading = -1;
	private TrafficObject[] m_trafficObjects;
	private boolean m_bCanMoveLeft = true;
	private boolean m_bCanMoveRight = true;
	private boolean m_bCanMoveUp = true;
	private boolean m_bTargetSpeedLocked = false;
	private boolean m_bOvertaking = false;
	private boolean m_bOvertakeLeft = false;
	
	private Random m_random = new Random();
    
	private Image m_imgCarImage;
    
    @Override
    public void update()
    {
		checkEdgeCollision();
		checkTrafficCollision();
		processInput();
		tryOvertaking();
		adjustSpeed();
    }
    
    @Override
    public void draw(Graphics g)
    {
		if (null == m_imgCarImage)
		{
			m_imgCarImage = Toolkit.getDefaultToolkit().getImage("adaptivecruisecontrol/main_car.png");
		}
		
		Graphics2D g2 = (Graphics2D)g; 
		g2.drawImage(m_imgCarImage, (int)m_fXPos - CAR_WIDTH / 2, (int)m_fYPos - CAR_HEIGHT / 2, null);
		g2.finalize();
		
		/*
		g.setColor(new Color(0, 0, 255));
		g.fillRect((int)m_fXPos - CAR_WIDTH / 2, (int)m_fYPos - CAR_HEIGHT / 2, CAR_WIDTH, CAR_HEIGHT);
		
		g.setColor(new Color(0, 255, 255));
		g.fillRect((int)m_fXPos - 3 * CAR_WIDTH / 8, (int)m_fYPos - CAR_HEIGHT / 10, 3 * CAR_WIDTH / 4, CAR_HEIGHT / 3);
		
		g.setColor(new Color(255, 255, 255));
		g.fillOval((int)m_fXPos - 2 * CAR_WIDTH / 5, (int)m_fYPos - 2 * CAR_HEIGHT / 5, CAR_WIDTH / 5, CAR_HEIGHT / 5);
		g.fillOval((int)m_fXPos + 1 * CAR_WIDTH / 5, (int)m_fYPos - 2 * CAR_HEIGHT / 5, CAR_WIDTH / 5, CAR_HEIGHT / 5);
		*/
    }
    
    public void setXPos(int newX)
    {
	m_fXPos = newX;
    }
    
    public void setYPos(int newY)
    {
	m_fYPos = newY;
    }
    
    public int getXPos()
    {
	return (int)m_fXPos;
    }
    
    public int getYPos()
    {
	return (int)m_fYPos;
    }
    
    public float getSpeed()
    {
		return m_fSpeed;
    }
	
	public float getTargetSpeed()
	{
		return m_fTargetSpeed;
	}
	
    public void lockTargetSpeed(boolean lock)
    {
		m_bTargetSpeedLocked = lock;
		m_fTargetSpeed = m_fSpeed;
    }
	
	public void lockTargetPosition()
	{
		m_fTargetXPos = m_fXPos;
	}
	
	public void setTrafficObjects(TrafficObject[] traffic)
	{
		m_trafficObjects = traffic;
	}
    
    public int getSensorReading()
    {
		return m_nSensorReading;
    }
    
	public void resetSensor()
	{
		m_nSensorReading = -1;
	}
	
    public void sensorDetect(int x, int y)
    {
		int value = (int)m_fYPos - y - CAR_HEIGHT;
		
		if (!(value > SENSOR_RANGE ||
			  value < 0 ||
			  Math.abs((int)m_fXPos - x) > CAR_WIDTH))
		{
			if (m_nSensorReading < 0 || value < m_nSensorReading)
			m_nSensorReading = value;
		}
    }
	
	private void checkEdgeCollision()
	{
		m_bCanMoveLeft = (int)m_fXPos > 2 * DashedStripe.EDGE_OFFSET + CAR_WIDTH / 2;
		m_bCanMoveRight = ((int)m_fXPos < Main.WIDTH - 2 * DashedStripe.EDGE_OFFSET - CAR_WIDTH / 2);
		m_bCanMoveUp = true;
	}
	
	private void checkTrafficCollision()
	{
		if (m_bCanMoveLeft)
		{
			for (int index = 0; index < m_trafficObjects.length; index++)
			{
				if (!(Math.abs((int)m_fYPos - m_trafficObjects[index].getYPos()) > CAR_HEIGHT ||
					(int)m_fXPos < m_trafficObjects[index].getXPos() ||
					(int)m_fXPos - m_trafficObjects[index].getXPos() > CAR_WIDTH))
					{
						m_bCanMoveLeft = false;
						break;
					}
			}
		}
		
		if (m_bCanMoveRight)
		{
			for (int index = 0; index < m_trafficObjects.length; index++)
			{
				if (!(Math.abs((int)m_fYPos - m_trafficObjects[index].getYPos()) > CAR_HEIGHT ||
					m_trafficObjects[index].getXPos() < (int)m_fXPos ||
					m_trafficObjects[index].getXPos() - (int)m_fXPos > CAR_WIDTH))
					{
						m_bCanMoveRight = false;
						break;
					}
			}
		}
		
		for (int index = 0; index < m_trafficObjects.length; index++)
		{
			if (m_trafficObjects[index].getYPos() < m_fYPos &&
				Math.abs(m_trafficObjects[index].getYPos() - m_fYPos) <= CAR_HEIGHT &&
				Math.abs(m_trafficObjects[index].getXPos() - m_fXPos) <= CAR_WIDTH)
				{
					m_bCanMoveUp = false;
					break;
				}
		}
	}
	
	private void processInput()
	{
		int steering = 0;
		int speed = 0;
		
		if (Main.ACCELEROMETER_ON)
		{
			try
			{
				if (!AccelerometerSensor.isEnabled())
			  {
				  AccelerometerSensor.enableSensor(true);
			  }
			  
			  steering = AccelerometerSensor.getData()[0];
			  speed = AccelerometerSensor.getData()[1];
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			
			float posIncrement = (float)steering / 4000;
			m_fXPos += (posIncrement > 0 && m_bCanMoveRight) || (posIncrement < 0 && m_bCanMoveLeft) ? posIncrement : 0; 
			
			float speedIncrement = (float) speed / 64000;
			m_fTargetSpeed += m_bTargetSpeedLocked ? 0 : speedIncrement;
		}
		else
		{
			m_fXPos += (m_bCanMoveLeft ? (Left ? -KEY_SENSITIVITY : 0) : 0) +
					(m_bCanMoveRight ? (Right ? KEY_SENSITIVITY : 0) : 0);
			m_fYPos += (Up ? -KEY_SENSITIVITY : 0) +
				   (Down ? KEY_SENSITIVITY : 0);
				   
			m_fTargetSpeed += m_bTargetSpeedLocked ? 0 : (Accelerate ? 1 : -1) * ACCELERATION;
			m_fTargetSpeed += (Brake ? -2 : 0) * ACCELERATION;
		}
	}
	
	private void tryOvertaking()
	{
		if (AutoOvertaking && AutoPilot)
		{
			if (!Left && !Right && !Brake && !Up && m_fTargetSpeed > m_fSpeed * 1.01f)
			{
				if (!m_bOvertaking)
				{
					m_bOvertaking = true;
					m_bOvertakeLeft = m_random.nextFloat() > 0.5f;
				}
			}
			else
			{
				m_bOvertaking = false;
			}
			
			if (m_bOvertaking)
			{
				//float currentOvertakeDistance = Math.abs(m_fXPos - m_fTargetXPos);
				//boolean stopSteering = currentOvertakeDistance > CarObject.CAR_WIDTH * 2.0f;
				
				boolean stopSteering = m_nSensorReading < 0 || (m_bOvertakeLeft && !m_bCanMoveLeft) || (!m_bOvertakeLeft && !m_bCanMoveRight);
				
				if (stopSteering && m_nSensorReading >= 0)
				{
					m_bOvertakeLeft = !m_bOvertakeLeft;
					stopSteering = false;
				}
				
				if (m_bOvertakeLeft)
				{
					if (m_bCanMoveLeft)
					{
						m_fXPos -= stopSteering ? 0 : KEY_SENSITIVITY;
					}
				}
				
				if (!m_bOvertakeLeft)
				{
					if (m_bCanMoveRight)
					{
						m_fXPos += stopSteering ? 0 : KEY_SENSITIVITY;
					}
				}
			}
			else if (((m_fXPos - m_fTargetXPos < 0 && m_bCanMoveRight) || (m_fXPos - m_fTargetXPos > 0 && m_bCanMoveLeft)))
			{
				m_fXPos = trackValueAbsolute(m_fXPos, m_fTargetXPos);
			}
		}
		else
		{
			m_bOvertaking = false;
		}
	}
	
	private void adjustSpeed()
	{
		m_fTargetSpeed = clampSpeed(m_fTargetSpeed);
		
		float sensorFactor = 1.0f;
		
		if (m_nSensorReading >= 0)
		{
			sensorFactor = (float)m_nSensorReading / SENSOR_RANGE;
			
			// Emergency breaking
			if (m_nSensorReading >= 0 && m_nSensorReading < MIN_SAFE_DISTANCE)
			{
				m_fSpeed *= 0.5f;
			}
		}
		
		m_fSpeed = trackValueAbsolute(m_fSpeed, m_fTargetSpeed * (float)(Math.max((1 + 0.3f * Math.log(sensorFactor)), 0)));
		m_fSpeed = clampSpeed(m_fSpeed);
	}
	
	private float clampSpeed(float speed)
	{
		return Math.max(0, Math.min(TOP_SPEED, speed));
	}
	
	private float trackValueAbsolute(float value, float target)
	{
		return value + ((target - value) > 0 ? 1 : -1) * SPEED_TRACK_SENSITIVITY;
	}
	
	private float trackValueRelative(float value, float target)
	{
		return value + ((target - value)) * SPEED_TRACK_SENSITIVITY;
	}
}