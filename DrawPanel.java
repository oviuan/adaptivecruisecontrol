package adaptivecruisecontrol;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class DrawPanel extends JPanel
{
	public static final int FPS_MEAN_SIZE = 30;
	
      private CarObject m_carObject;
      private java.util.List<IGameObject> m_arrGameObjects = new ArrayList<IGameObject>();
	  private long m_nLastMillis;
	  private long m_nFps;
	  private long[] m_arrFps = new long[FPS_MEAN_SIZE];
      
      public DrawPanel() { super(); }
      public DrawPanel(boolean isDoubleBuffered) { super(isDoubleBuffered); }
      
      public void registerObject(IGameObject go)
      {
		m_arrGameObjects.add(go);
      }
	  
	  public void registerObjects(IGameObject[] gos)
	  {
		  for (int index = 0; index < gos.length; index++)
		  {
			  registerObject(gos[index]);
		  }
	  }
      
      public void unregisterObject(IGameObject go)
      {
		m_arrGameObjects.remove(go);
      }
	  
	  public void unregisterObjects(IGameObject[] gos)
	  {
		  for (int index = 0; index < gos.length; index++)
		  {
			  unregisterObject(gos[index]);
		  }
	  }
   
      @Override
      public void paintComponent(Graphics g)
      {
		super.paintComponent(g);
		
		for (IGameObject go : m_arrGameObjects)
		{
			go.draw(g);
		}
		
		int speed = (int)(((float)m_carObject.getSpeed() / CarObject.TOP_SPEED) * 160);
		int targetSpeed = (int)(((float)m_carObject.getTargetSpeed() / CarObject.TOP_SPEED) * 160);
		
		g.setColor(m_nFps > 30 ? new Color(0, 255, 0) : new Color(255, 0, 0));
		g.drawString("FPS: " + m_nFps, 50, 20);
		
		g.setColor(new Color(0, 255, 0));
		g.drawString("Speed: " + speed + " kmph", 50, 40);
		
		g.setColor(m_carObject.AutoPilot ? new Color(0, 255, 0) : new Color(255, 0, 0));
		g.drawString("Cruise Control: " + (m_carObject.AutoPilot ? "ON" : "OFF"), 50, 60);
		
		g.setColor(m_carObject.AutoOvertaking ? new Color(0, 255, 0) : new Color(255, 0, 0));
		g.drawString("Overtaking: " + (m_carObject.AutoOvertaking ? "ON" : "OFF"), 50, 80);
		
		g.setColor(new Color(0, 255, 0));
		g.drawString("Sensor reading: " + m_carObject.getSensorReading(), 50, 100);
		
		g.setColor(Main.ACCELEROMETER_ON ? new Color(0, 255, 0) : new Color(255, 0, 0));
		g.drawString("Accelerometer: " + (Main.ACCELEROMETER_ON ? "ON" : "OFF"), 50, 120);
      }
      
      public void update()
      {
		long currentMillis = new Date().getTime();
		m_nFps = 1000 / (currentMillis - m_nLastMillis);
		m_nLastMillis = currentMillis;
		
		for (int index = 1; index < FPS_MEAN_SIZE; index++)
		{
			m_arrFps[index - 1] = m_arrFps[index];
		}
		m_arrFps[FPS_MEAN_SIZE - 1] = m_nFps;
		
		m_nFps = 0;
		for (int index = 0; index < FPS_MEAN_SIZE; index++)
		{
			m_nFps += m_arrFps[index];
		}
		
		m_nFps /= FPS_MEAN_SIZE;
		m_carObject.resetSensor();
		
		for (IGameObject go : m_arrGameObjects)
		{
			go.update();
		}
		
		repaint();
      }
      
      public void setCarObject(CarObject car)
      {
		m_carObject = car;
      }
}