package adaptivecruisecontrol;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

public class TrafficObject implements IGameObject
{
	public static boolean RANDOM_COLORS = false;
	
	private enum State
	{
		NONE,
		STOP,
		SLOW,
		FAST,
		STEER
	}
	private static final int m_sStates = State.values().length;
	private static ArrayList<TrafficObject> m_arrAllTrafic = new ArrayList<TrafficObject>();
	
	private float[] m_arrStateProb = new float[]
	{
		0.1f,
		0.2f,
		0.4f,
		0.7f,
		1.0f
	};
	
	private State m_enuState = State.SLOW;
	
	private float m_fCounter = 0.0f;
	private float m_fTargetCounter = 5.0f;
	
  private float m_fXPos = 100;
  private float m_fSpeed = 0;
	
	private int m_nCurrentLane = 0;
	private int m_nTargetXPos = 200;
	private int m_nTargetSpeed = 0;
	
	private int m_nYPos = 100;
	
	private boolean m_bCanMoveLeft;
	private boolean m_bCanMoveRight;
	private boolean m_bCanMoveUp;
	
    private Random m_random = new Random();
    private CarObject m_carObject;
	
	private Color m_colPrimary;
	private Color m_colSecondary;
	
	private Image m_imgCarImage;
    
    
		public TrafficObject()
		{
			changeColor();
			m_fCounter = m_random.nextFloat() * m_fTargetCounter;
			m_arrAllTrafic.add(this);
		}
		
		private void changeColor()
		{
			m_colPrimary = RANDOM_COLORS ? new Color((int)(m_random.nextFloat() * 255), (int)(m_random.nextFloat() * 255), (int)(m_random.nextFloat() * 255)) : new Color(255, 0, 0);
			m_colSecondary = RANDOM_COLORS ? new Color((int)(m_random.nextFloat() * 255), (int)(m_random.nextFloat() * 255), (int)(m_random.nextFloat() * 255)) : new Color(255, 255, 0);
		}
		
    @Override
    public void update()
    {
		CheckCollisions();
		
			m_fCounter += 0.1f;
			if (m_fCounter > m_fTargetCounter)
			{
				float randomFloat = m_random.nextFloat();
				State newState = randomFloat < m_arrStateProb[0] ? State.NONE :
								randomFloat < m_arrStateProb[1] ? State.STOP :
								randomFloat < m_arrStateProb[2] ? State.SLOW :
								randomFloat < m_arrStateProb[3] ? State.FAST :
								State.STEER;
				
				if (m_nTargetSpeed == 0 || m_enuState == State.STOP) newState = State.SLOW;
				if (!m_bCanMoveUp) newState = State.STEER;
				/*
				do
				{
					newState = State.values()[m_random.nextInt(m_sStates)];
				}
				while (newState == m_enuState);
				*/
				
				m_enuState = newState;
				m_fCounter = 0.0f;
				m_fTargetCounter = 10.0f + m_random.nextFloat() * 5;
				
				switch (m_enuState)
				{
					case STOP:
						m_nTargetSpeed = 0;
					break;
					case SLOW:
						m_nTargetSpeed = 4;
					break;
					case FAST:
						m_nTargetSpeed = 8;
					break;
					case STEER:
						pickRandomLane();
					break;
				}
			}
						
			m_fSpeed += (m_nTargetSpeed - m_fSpeed) * 0.05f;
			if (!m_bCanMoveUp)
			{
				m_fSpeed *= 0.5f;
			}
			
			float xDelta = (m_nTargetXPos - m_fXPos) * 0.02f;
			
			m_fXPos += (xDelta < 0 && m_bCanMoveLeft) || (xDelta > 0 && m_bCanMoveRight) ? xDelta : 0.0f; 
			
			m_nYPos -= (int)(m_fSpeed - m_carObject.getSpeed());
			
			if (m_nYPos > 2 * Main.HEIGHT)
			{
				m_nYPos = -Main.HEIGHT + 100;
				pickRandomLane();
				changeColor();
			}
			
			if (m_nYPos < -Main.HEIGHT)
			{
				m_nYPos = 2 * Main.HEIGHT - 100;
				pickRandomLane();
				changeColor();
			}
			
			m_carObject.sensorDetect((int)m_fXPos, m_nYPos);
    }
    
    @Override
    public void draw(Graphics g)
    {
		if (null == m_imgCarImage)
		{
			m_imgCarImage = Toolkit.getDefaultToolkit().getImage("adaptivecruisecontrol/traffic_car.png");
		}
		
		Graphics2D g2 = (Graphics2D)g; 
		g2.drawImage(m_imgCarImage, (int)m_fXPos - CarObject.CAR_WIDTH / 2, (int)m_nYPos - CarObject.CAR_HEIGHT / 2, null);
		g2.finalize();
		
		g.setColor(new Color(255, 255, 255));
		g.drawString("" + m_enuState, (int)m_fXPos - CarObject.CAR_WIDTH / 4, m_nYPos + CarObject.CAR_HEIGHT / 10);
		
		/*
			g.setColor(m_colPrimary);
			g.fillRect((int)m_fXPos - CarObject.CAR_WIDTH / 2, m_nYPos - CarObject.CAR_HEIGHT / 2, CarObject.CAR_WIDTH, CarObject.CAR_HEIGHT);
			
			g.setColor(m_colSecondary);
			g.fillRect((int)m_fXPos - 3 * CarObject.CAR_WIDTH / 8, m_nYPos - CarObject.CAR_HEIGHT / 10, 3 * CarObject.CAR_WIDTH / 4, CarObject.CAR_HEIGHT / 3);
			
			g.setColor(new Color(255, 255, 255));
			g.fillOval((int)m_fXPos - 2 * CarObject.CAR_WIDTH / 5, m_nYPos - 2 * CarObject.CAR_HEIGHT / 5, CarObject.CAR_WIDTH / 5, CarObject.CAR_HEIGHT / 5);
			g.fillOval((int)m_fXPos + 1 * CarObject.CAR_WIDTH / 5, m_nYPos - 2 * CarObject.CAR_HEIGHT / 5, CarObject.CAR_WIDTH / 5, CarObject.CAR_HEIGHT / 5);
			
			g.setColor(new Color(255, 255, 255));
			g.drawString("" + m_enuState, (int)m_fXPos - CarObject.CAR_WIDTH / 4, m_nYPos + CarObject.CAR_HEIGHT / 3 + 10);
		*/
			
			/*
			int speed = (int)((m_fSpeed / CarObject.TOP_SPEED) * 160);
			g.setColor(new Color(255, 0, 0));
			g.drawString("" + speed, (int)(m_fXPos - CarObject.CAR_WIDTH * 0.1f), m_nYPos + (int)(CarObject.CAR_HEIGHT * 0.1f));
			*/
    }
    
    public void setXPos(int newX)
    {
			m_fXPos = newX;
    }
    
    public void setYPos(int newY)
    {
		m_nYPos = newY;
    }
    
    public int getXPos()
    {
		return (int)m_fXPos;
    }
    
    public int getYPos()
    {
		return m_nYPos;
    }
    
    public void setSpeed(int speed)
    {
		m_fSpeed = speed;
    }
    
    public int getSpeed()
    {
		return (int)m_fSpeed;
    }
    
    public void setCarObject(CarObject car)
    {
		m_carObject = car;
    }
    
    public void pickRandomLane()
    {
			float randomLane;
			
			do
			{
				randomLane = 1 + m_random.nextInt(DashedStripe.LANES) - 0.5f;
			}
			while (Math.abs((int)randomLane - m_nCurrentLane) != 1);
			
			float totalWidth = (Main.WIDTH - 2 * DashedStripe.EDGE_OFFSET) / (DashedStripe.LANES);
			
			m_nCurrentLane = (int)randomLane;
			m_nTargetXPos = (int)(randomLane * totalWidth) + DashedStripe.EDGE_OFFSET;
    }
		
		public void pickRandomYPosition()
		{
			m_nYPos = (int)(-Main.HEIGHT + m_random.nextFloat() * Main.HEIGHT * 2.0f);
		}
		
		private void CheckCollisions()
		{
			m_bCanMoveLeft = true;
			m_bCanMoveRight = true;
			m_bCanMoveUp = true;
			
			float safeDistanceX = CarObject.CAR_WIDTH * 1.2f;
			float safeDistanceY = CarObject.CAR_HEIGHT * 1.2f;
			
			for (int index = 0; index < m_arrAllTrafic.size(); index++)
			{
				if (m_arrAllTrafic.get(index) == this)
				{
					continue;
				}
				
				if (m_bCanMoveLeft &&
						m_arrAllTrafic.get(index).getXPos() < m_fXPos &&
						Math.abs(m_arrAllTrafic.get(index).getXPos() - m_fXPos) <= safeDistanceX &&
						Math.abs(m_arrAllTrafic.get(index).getYPos() - m_nYPos) <= safeDistanceY)
						{
							m_bCanMoveLeft = false;
						}
				if (m_bCanMoveRight &&
						m_arrAllTrafic.get(index).getXPos() > m_fXPos &&
						Math.abs(m_arrAllTrafic.get(index).getXPos() - m_fXPos) <= safeDistanceX &&
						Math.abs(m_arrAllTrafic.get(index).getYPos() - m_nYPos) <= safeDistanceY)
						{
							m_bCanMoveRight = false;
							m_bCanMoveLeft = true;
						}
				if (m_bCanMoveUp &&
						m_arrAllTrafic.get(index).getYPos() < m_nYPos &&
						Math.abs(m_arrAllTrafic.get(index).getYPos() - m_nYPos) <= safeDistanceY &&
						Math.abs(m_arrAllTrafic.get(index).getXPos() - m_fXPos) <= safeDistanceX)
						{
							m_bCanMoveUp = false;
						}
				if (!m_bCanMoveLeft && !m_bCanMoveRight && !m_bCanMoveUp)
				{
					break;
				}
			}
			
			
			if (m_bCanMoveLeft &&
						m_carObject.getXPos() < m_fXPos &&
						Math.abs(m_carObject.getXPos() - m_fXPos) <= safeDistanceX &&
						Math.abs(m_carObject.getYPos() - m_nYPos) <= safeDistanceY)
						{
							m_bCanMoveLeft = false;
						}

			if (m_bCanMoveRight &&
						m_carObject.getXPos() > m_fXPos &&
						Math.abs(m_carObject.getXPos() - m_fXPos) <= safeDistanceX &&
						Math.abs(m_carObject.getYPos() - m_nYPos) <= safeDistanceY)
						{
							m_bCanMoveRight = false;
						}

			if (m_bCanMoveUp &&
						m_carObject.getYPos() < m_nYPos &&
						Math.abs(m_carObject.getYPos() - m_nYPos) <= safeDistanceY &&
						Math.abs(m_carObject.getXPos() - m_fXPos) <= safeDistanceX)
						{
							m_bCanMoveUp = false;
						}
		}
}