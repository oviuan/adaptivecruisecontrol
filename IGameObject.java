package adaptivecruisecontrol;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public interface IGameObject
{
  public void update();
  public void draw(Graphics g);
}