package adaptivecruisecontrol;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class Main extends JFrame
{
   public static boolean ACCELEROMETER_ON = false;
   public static String WINDOW_TITLE = "Cruise Control";
   public static int WIDTH = 640;
   public static int HEIGHT = 480;
   public static int MILLIS_PER_FRAME = 15;
   public static int KEY_SENSITIVITY = 2;
	 public static int MAX_TRAFFIC_OBJECTS = 3;
   
   private DrawPanel m_drawPanel;
   private CarObject m_carObject;
   private TrafficObject[] m_trafficObjects;
   private DashedStripe m_stripes;
   
   
   public Main()
   {
      Container contentPane = getContentPane();
      contentPane.setLayout(new FlowLayout());
      
      m_drawPanel = new DrawPanel(true);
      m_drawPanel.setBackground(new Color(50, 50, 50));
      m_drawPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));

      contentPane.add(m_drawPanel);
      
      m_carObject = new CarObject();
      m_carObject.setXPos(WIDTH / 2);
      m_carObject.setYPos(HEIGHT - 100);
      
      m_trafficObjects = new TrafficObject[MAX_TRAFFIC_OBJECTS];
			for (int index = 0; index < MAX_TRAFFIC_OBJECTS; index++)
			{
				m_trafficObjects[index] = new TrafficObject();
				m_trafficObjects[index].setCarObject(m_carObject);
				m_trafficObjects[index].pickRandomLane();
				m_trafficObjects[index].pickRandomYPosition();
			}
      
      m_stripes = new DashedStripe();
      m_stripes.setCarObject(m_carObject);
	  
	  	m_carObject.setTrafficObjects(m_trafficObjects);
      
      
      m_drawPanel.setCarObject(m_carObject);
      m_drawPanel.registerObject(m_stripes);
      m_drawPanel.registerObjects(m_trafficObjects);
      m_drawPanel.registerObject(m_carObject);
      
      this.addKeyListener(new KeyListener()
      {
	  @Override
	  public void keyTyped(KeyEvent e) {}
	  
	  @Override
	  public void keyReleased(KeyEvent e)
	  {
	      switch (e.getKeyCode())
	      {
		  case KeyEvent.VK_UP:
		    m_carObject.Accelerate = false;
		  break;
		  
		  case KeyEvent.VK_DOWN:
		    m_carObject.Brake = false;
		  break;
		  
		  case KeyEvent.VK_LEFT:
		    m_carObject.Left = false;
		  break;
		  
		  case KeyEvent.VK_RIGHT:
		    m_carObject.Right = false;
		  break;
			
			case KeyEvent.VK_A:
					m_carObject.AutoPilot = !m_carObject.AutoPilot;
					m_carObject.lockTargetSpeed(m_carObject.AutoPilot);
				break;
				
		  case KeyEvent.VK_C:
				ACCELEROMETER_ON = !ACCELEROMETER_ON;
			break;
			case KeyEvent.VK_O:
				m_carObject.AutoOvertaking = !m_carObject.AutoOvertaking;
				m_carObject.lockTargetPosition();
			break;
	      }
	  }
	  
	  @Override
	  public void keyPressed(KeyEvent e)
	  {
	      switch (e.getKeyCode())
	      {
				case KeyEvent.VK_UP:
					m_carObject.Accelerate = true;
				break;
				
				case KeyEvent.VK_DOWN:
					m_carObject.Brake = true;
					m_carObject.AutoPilot = false;
					m_carObject.lockTargetSpeed(false);
				break;
				
				case KeyEvent.VK_LEFT:
				m_carObject.Left = true;
				break;
				
				case KeyEvent.VK_RIGHT:
					m_carObject.Right = true;
				break;
	      }
	  }
      });
 
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setTitle(WINDOW_TITLE);
      setSize(WIDTH, HEIGHT);
      Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
      setLocation(dim.width/2-this.getSize().width/2,
		  dim.height/2-this.getSize().height/2);
      setVisible(true);
      
      javax.swing.Timer t = new javax.swing.Timer(
		MILLIS_PER_FRAME,
		new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				m_drawPanel.update();
			}
		});
		  
		t.start();
   }
 
   public static void main(String[] args)
   {
      SwingUtilities.invokeLater(new Runnable()
      {
         @Override
         public void run()
         {
            new Main();
         }
      });
   }
}